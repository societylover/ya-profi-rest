const express = require('express')
const swaggerJSDoc = require('swagger-jsdoc')
const swaggerUI = require('swagger-ui-express')

const PromoController = require('./controller/PromoController.js')
const controller = new PromoController()

const PORT = 8080
const app = express()

const swaggerOptions = {
    swaggerDefinition: {
        title: 'Promo API',
        version: '1.0.0'
    },
    apis: ['app.js']
}

const swaggerDocs = swaggerJSDoc(swaggerOptions)
app.use(express.json())
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocs ))

app.post('/promo', async(req, res)=>{
    let id = controller.postPromo(req.body)
    res.send({id})
}) 


/** 
 * @swagger
 * /promo:
 *    get: 
 *      description: 
 *          Данные о всех промоакциях 
 *      responses:
 *          200:
 *              description: Данные о промоакциях (кратко) в виде массива json
*/
app.get('/promo', async(req, res)=>{
    let promos = controller.getPromos()
    res.send(promos)
})

/** 
 * @swagger
 * /promo/{id}:
 *    get: 
 *      description: 
 *         Данные о промоакции (полные с указанием призов и участников)
 *      parameters:
 *        - name: id
 *          in: path
 *          description: id промоакции
 *          type: integer
 *          required: true
 *      responses:
 *          200:
 *              description: Промо было найдено и отправлено в json
 *          404:
 *              description: Промо не было найдено
*/
app.get('/promo/:id', async (req, res)=>{
    let promo = controller.getPromo(parseInt(req.params.id))
    if (promo) res.send(promo)
    else res.status(404).send()
})

/** 
 * @swagger
 * /promo/{id}:
 *    put: 
 *      description: 
 *          Обновление данных у промоакции
 *      parameters:
 *        - name: id
 *          in: path
 *          description: id записи
 *          type: integer
 *          required: true
 *        - in: body
 *          name: promo
 *          description: Новая запись
 *          type: object
 *          required:
 *            - name 
 *          properties:
 *            name:
 *             type: string
 *            description:
 *             type: string 
 *      responses:
 *          200:
 *              description: Промо было найдено и обновлено
 *          401:
 *              description: Промо было создано
*/
app.put('/promo/:id', async (req, res)=>{ 
    console.log(req.body)
    let updateNew = controller.putPromo(parseInt(req.params.id), req.body)
    if (updateNew === 0) res.status(200).send()
    else res.status(201).send()
})

/** 
 * @swagger
 * /promo/{id}:
 *    delete: 
 *      description: 
 *          Удаление промо с указанным id
 *      parameters:
 *          - name: id
 *            in: path
 *            description: id промо
 *            type: integer
 *            required: true
 *      responses:
 *          200:
 *              description: Промо было удалена
 *          404:
 *              description: Промо не было удалено
*/
app.delete('/promo/:id', async(req, res)=> {
    let isDeleted = controller.deletePromo(parseInt(req.params.id))
    if (isDeleted) res.status(200).send()
    else res.status(404).send()
})

/** 
 * @swagger
 * /promo/{promoId}/participant:
 *    post: 
 *      description: 
 *          Создание нового участника для промо
 *      parameters:  
 *          - in: body
 *            name: note
 *            description: Новый участник
 *            type: object
 *            required:
 *              - name 
 *            properties:
 *              name:
 *               type: string 
 *      responses:
 *          200:
 *              description: Участник добавлен и получен его id
 *          400:
 *              description: Участник не добавлен
*/
app.post('/promo/:id/participant', async(req, res)=>{
    let participantId = controller.postParticipant(parseInt(req.params.id), req.body)
    if (participantId === -1) res.status(400).send()
    else res.send({participantId})
})

/** 
 * @swagger
 * /promo/{promoId}/participant/{participantId}:
 *    delete: 
 *      description: 
 *          Удаление участника с participantId из промо с promoId
 *      parameters:
 *          - name: promoId
 *            in: path
 *            description: id промо
 *            type: integer
 *            required: true
 *          - name: participantId
 *            in: path
 *            description: id участника
 *            type: integer
 *            required: true
 *      responses:
 *          200:
 *              description: Запись была удалена
 *          404:
 *              description: Удаление записи не было произведено
*/
app.delete('/promo/:promoId/participant/:participantId', async(req, res)=>{
    let participantIsDeleted = controller.deleteParticipant(parseInt(req.params.promoId), parseInt(req.params.participantId))
    if (participantIsDeleted) res.status(200).send()
    else res.status(404).send()
})

/** 
 * @swagger
 * /promo/{id}/prize:
 *    post: 
 *      description: 
 *          Создание нового приза для промоакции с id
 *      parameters:
 *          - name: id
 *            in: path
 *            description: id промо
 *            type: integer
 *            required: true
 *          - in: body
 *            name: prize
 *            description: описание приза
 *            type: object
 *            required:
 *              - description 
 *            properties: 
 *              description:
 *               type: string 
 *      responses:
 *          200:
 *              description: Запись была создана и вернулся id ее
 *          400:
 *              description: Запись не была создана
*/
app.post('/promo/:id/prize', async(req, res)=>{
    let prizeId = controller.postPrize(parseInt(req.params.id), req.body)
    if (prizeId === -1) res.status(400).send()
    else res.status(200).send({prizeId})
})

/** 
 * @swagger
 * /promo/{promoId}/prize/{prizeId}:
 *    delete: 
 *      description: 
 *          Удаление приза с указанным prizeId из промо с promoId 
 *      parameters:
 *          - name: promoId
 *            in: path
 *            description: id промо
 *            type: integer
 *            required: true
 *          - name: prizeId
 *            in: path
 *            description: id приза
 *            type: integer
 *            required: true
 *      responses:
 *          200:
 *              description: Запись была удалена
 *          404:
 *              description: Удаление записи не было произведено
*/
app.delete('/promo/:promoId/prize/:prizeId', async (req, res)=>{
    let isDeleted = controller.deletePrize(parseInt(req.params.promoId), parseInt(req.params.prizeId))
    if (isDeleted) res.status(200).send()
    else res.status(404).send()
})

/** 
 * @swagger
 * /promo/{id}/raffle:
 *    post: 
 *      description: 
 *          Проведение розыгрыша
 *      parameters:  
 *          - name: id
 *            in: path
 *            description: id промо
 *            type: integer
 *            required: true
 *      responses:
 *          200:
 *              description: Был проведен розыгрыш и получены данные о розыгрыше
*/
app.post('/promo/:id/raffle', async(req, res)=>{
    let raffle = controller.postRaffle(parseInt(req.params.id))
    if (raffle) res.status(200).send(raffle)
    else res.status(400).send()
})


app.listen(PORT, ()=>{ console.log(`listen on port: ${PORT}`); })