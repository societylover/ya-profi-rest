const prize = require('./Prize.js')
const participant = require('./Participant.js')
const drawresult = require('./DrawResult.js') 

// Проводимая промоакция
class Promo { 

    constructor(id, json) {
        this.id = id
        this.name = json["name"]
        this.desc = json["description"]
        this.prizeId = 1
        this.participantId = 1
        this.prizes = new Map()
        this.participants = new Map()
    } 

    getID() {
        return this.id
    }

    updatePromo(updateValues) {
        if (updateValues["name"] && updateValues["name"] !== "") {
            this.name = updateValues["name"]
        }
        this.desc = updateValues["description"]
    }

    // Добавляем приз с новым id и Описанием
    addPrize(desc) {
        let newId = this.prizeId++
        this.prizes.set(newId, new prize(newId, desc))
        return newId
    }

    // true если нашел и удалил
    deletePrizeByID(id) {
        let deleteResult = this.prizes.delete(id)
        return deleteResult
    }

    addParticipant(name) {
        let newId = this.participantId++
        this.participants.set(newId, new participant(newId, name))
        return newId
    }

    deleteParticipantByID(id) {
        try {
            let deleteResult = this.participants.delete(id)
            return deleteResult
        } catch(e) {
            return false
        } 
    }

    // Данные промоакции при запросе по ID
    getShortPromoData() { 
        return this.toJSON()
    }

    toShortJSON() {
        return { "id": this.id, "name": this.name, "description": this.desc }
    } 

    getFullPromoData() {
        let shortJSON = this.toShortJSON()
        shortJSON.prizes = new Array()  
        for (let prize of this.prizes.values()) {
            shortJSON.prizes.push(prize.toJSON())
        }
        shortJSON.participants = new Array() 
        for (let participant of this.participants.values()) {
            shortJSON.participants.push(participant.toJSON())
        }  
        return shortJSON
    }

    shuffle(shuffleArray) {
        let array = shuffleArray
        for (let i = array.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1)); 
            [array[i], array[j]] = [array[j], array[i]];
        }
        return array
    }

    makeRaffle() {
        if (this.participants.size === 0) return null

        if (this.participants.size != this.prizes.size) {
            return null
        }

        let shuffledPrizes = this.shuffle(Array.from(this.prizes, ([key, value]) => (value)))
        let shuffledParticipants = this.shuffle(Array.from(this.participants, ([key, value]) => (value)))

        let result = new Array() 
        for (let i = 0; i < shuffledPrizes.length; i++) {
            console.log(shuffledParticipants[i])
            result.push({"winner": shuffledParticipants[i].toJSON(), "prize": shuffledPrizes[i].toJSON()})
        }

        return result
    }
}

module.exports = Promo