class Prize {
    constructor(id, desc) { 
        this.id = id
        this.desc = desc
    } 

    getDesc() {
        return this.desc
    }
    
    getID() {
        return this.id
    }

    toJSON() {
        return {"id": this.id, "description": this.desc }
    }
}

module.exports = Prize