const Participant = require('./Participant.js')
const Prize = require('./Prize.js')

class DrawResult {
    constructor (participant, prize) {
        this.participant = participant
        this.prize = prize
    }

    toJSON() {
        return { "winner": this.participant.toJSON(), "prize": this.prize.toJSON() }
    }
}

module.exports = DrawResult