const Promo = require('../model/Promo.js') 

class PromoController {
    constructor() {
        this.promos = new Map()
    }

    // POST (1)
    postPromo(promoData) {
        let newID = this.promos.size + 1
        this.promos.set(newID, new Promo(newID, promoData))  
        return newID
    }

    // GET (2)
    getPromos() {
        return Array.from(this.promos, ([key, value])=>(value.toShortJSON()))
    }

    // GET (3)
    getPromo(id) {  
        let promo = this.promos.get(id)
        if (!promo) {  
            return null 
        } 
        return promo.getFullPromoData()
    }

    // PUT (4)
    putPromo(id, json) {
        let promo = this.promos.get(id)
        if (promo) {
            promo.updatePromo(json) 
            return 0
        } else {
            this.postPromo(json)
            return 1
        } 
    }

    // DELETE (5)
    deletePromo(id) {
        let isPromoDeleted = this.promos.delete(id)
        return isPromoDeleted
    } 

    // POST PARTICIPANT (6)
    postParticipant(idPromo, participantData) {
        let promo = this.promos.get(idPromo)
        if (!promo) return -1
 
        return promo.addParticipant(participantData["name"])
    } 

    // DELETE PARTICIPANT (7)
    deleteParticipant(idPromo, idParticipant) {
        let promo = this.promos.get(idPromo)
        if (!promo) return false 
        return promo.deleteParticipantByID(idParticipant)
    }

    // POST PRIZE (8)
    postPrize(idPromo, prize) {
        let promo = this.promos.get(idPromo)
        if (!promo) return -1

        return promo.addPrize(prize["description"])
    }

    // DELETE PRIZE (9)
    deletePrize(idPromo, prizeId) {
        let promo = this.promos.get(idPromo)
        if (!promo) return false

        return promo.deletePrizeByID(prizeId)
    }

    // POST RAFFLE (10)
    postRaffle(idPromo) {
        let promo = this.promos.get(idPromo)
        if (!promo) return null

        return promo.makeRaffle()
    }
}

module.exports = PromoController